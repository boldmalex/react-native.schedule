import React, { useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
} from "react-native";
import ScheduleHeader from "./schedule-header";
import { useParams } from "react-router-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import data from "../data/schedule-data.js";
import timeData from "../data/time-data.js";
import groupValues from "../data/education_values";
import { dayValues } from "../data/time-data.js";

export default function ScheduleShow() {
  const { idGroup } = useParams();
  const [currentDay, setCurrentDay] = useState(1);

  const getTimeValue = (id) => {
    return timeData.filter((item) => item.id === id)[0].time;
  };

  const btnForwardPress = () => {
    if (currentDay !== 7) setCurrentDay(currentDay + 1);
  };

  const btnBackPress = () => {
    if (currentDay !== 1) setCurrentDay(currentDay - 1);
  };

  const showTimes = [0, 1, 2, 3, 4, 5].map((time_id) => {
    return (
      <Row style={styles.row} size={1}>
        <Text style={styles.row_text}>{getTimeValue(time_id)}</Text>
      </Row>
    );
  });

  let skip;
  const showElements = (valColumn) => {
    return [0, 1, 2, 3, 4, 5].map((time_id) => {
      if (skip && skip > 0) {
        skip--;
      } else {
        // Получаем текущие элементы
        const elementsInCurrentRow = data.filter(
          (item) =>
            item.row_nums.some((row) => row === time_id) &&
            item.col_nums.some((col) => col === valColumn) &&
            item.group_id == idGroup &&
            item.day_id === currentDay
        );
        let name = "";
        let sizeRow = 1;

        // Пустая строка
        if (elementsInCurrentRow.length !== 0) {
          name = elementsInCurrentRow[0].name;
          sizeRow = elementsInCurrentRow[0].row_nums.length;
          skip = elementsInCurrentRow[0].row_nums.length - 1;
        }

        return (
          <Row style={styles.row} size={sizeRow}>
            <Text style={styles.row_text}>{name}</Text>
          </Row>
        );
      }
    });
  };

  const showLeft = showElements(1);
  const showRight = showElements(2);

  const showDay = () => {
    return dayValues.filter((item) => item.day === currentDay)[0].value;
  };

  const showGroup = () => {
    return groupValues.filter((item) => item.value === idGroup)[0].label;
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScheduleHeader />
      <View style={styles.caption_container}>
        <Text style={styles.caption_text}>{showGroup()}</Text>
      </View>
      <View style={styles.content_container}>
        <View style={styles.caption_container}>
          <Text style={styles.caption_text}>{showDay()}</Text>
        </View>
        <Grid style={styles.grid}>
          <Col style={styles.col} size={1}>
            <Row style={styles.row} size={1}>
              <Text style={styles.row_text}>#</Text>
            </Row>
            {showTimes}
          </Col>
          <Col style={styles.col} size={1.7}>
            <Row style={styles.row} size={1}>
              <Text style={styles.row_text}>Числитель</Text>
            </Row>
            {showLeft}
          </Col>
          <Col style={styles.col} size={1.7}>
            <Row style={styles.row} size={1}>
              <Text style={styles.row_text}>Знаменатель</Text>
            </Row>
            {showRight}
          </Col>
        </Grid>
        <View style={styles.navigate_container}>
          <View style={styles.navigate_container_content}>
            <TouchableOpacity
              onPress={btnBackPress}
              style={styles.btn_navigate}
            >
              <Text style={styles.btn_text_navigate}>{"<"}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.navigate_container_content}>
            <TouchableOpacity
              onPress={btnForwardPress}
              style={styles.btn_navigate}
            >
              <Text style={styles.btn_text_navigate}>{">"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
    margin: 5,
    marginTop: 0,
  },
  content_container: {
    marginTop: 20,
    flex: 1,
    width: "100%",
    height: 100,
  },
  grid: {
    marginLeft: 5,
    marginRight: 5,
  },
  row: {
    borderTopWidth: 1,
    borderTopColor: "lightgray",
    borderBottomWidth: 1,
    borderBottomColor: "lightgray",
    alignItems: "center",
    justifyContent: "center",
  },
  row_text: {
    fontFamily: "c_gothic",
  },
  col: {
    borderLeftWidth: 1,
    borderLeftColor: "lightgray",
    borderRightWidth: 1,
    borderRightColor: "lightgray",
  },
  caption_container: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  caption_text: {
    color: "black",
    fontSize: 20,
    fontFamily: "c_gothic",
  },
  navigate_container: {
    flexDirection: "row",
    width: "100%",
  },
  navigate_container_content: {
    flex: 1,
    alignItems: "center",
  },
  btn_text_navigate: {
    color: "black",
    fontSize: 30,
  },
  btn_navigate: {
    //width: "100%",
    //height: "100%",
  },
});
