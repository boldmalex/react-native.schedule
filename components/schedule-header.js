import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  Image,
  Text,
  TouchableOpacity,
} from "react-native";
import { useNavigate } from "react-router-native";

export default function ScheduleHeader() {
  const navigate = useNavigate();

  const btnBackPress = () => {
    navigate(`/`);
  };

  return (
    <SafeAreaView style={styles.header_container}>
      <Image
        style={styles.header_picture}
        source={require("../assets/logo3.png")}
      />
      <TouchableOpacity style={styles.header_back_btn} onPress={btnBackPress}>
        <Text style={styles.header_back_btn_text}>{"<"}</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header_container: {
    backgroundColor: "#33EF71",
    alignItems: "center",
    justifyContent: "center",
    height: 90,
    shadowColor: "#33EF71",
    shadowOpacity: 0.61,
    shadowOffset: { width: 0, height: 60 },
    shadowRadius: 2,
    elevation: 1,
  },
  header_picture: {
    marginTop: 20,
    width: 40,
    height: 40,
  },
  header_back_btn: {
    opacity: 10,
    position: "absolute",
    color: "steelblue",
    left: 20,
    top: 40,
    width: 50,
    height: 50,
  },
  header_back_btn_text: {
    color: "white",
    fontSize: 30,
  },
});
