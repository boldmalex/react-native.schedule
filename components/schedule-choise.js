import React, { useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  Button,
  Text,
  TouchableOpacity,
} from "react-native";
import PageHeader from "./page-header";
import { RadioButton } from "react-native-paper";
import { useNavigate } from "react-router-native";
import RNPickerSelect from "react-native-picker-select";
import groupValues, {
  courseValues,
  cafedraValues,
} from "../data/education_values";
import localstorage from "../data/local-storage";

export default function ScheduleChoise() {
  const [memberRadioChecked, setMemberRadioChecked] = useState(
    localstorage.isStored
  );
  console.log(localstorage);
  const [cafedra, setCafedra] = useState(localstorage.cafedraId);
  const [course, setCourse] = useState(localstorage.courseId);
  const [group, setGroup] = useState(localstorage.groupId);
  const navigate = useNavigate();

  const onLoadPress = () => {
    if (group > 0) navigate(`schedule/${group}`);
  };

  const changeCafedra = (value) => {
    if (memberRadioChecked) {
      localstorage.cafedraId = value;
    }
    setCafedra(value);
  };
  const changeCourse = (value) => {
    if (memberRadioChecked) {
      localstorage.courseId = value;
    }
    setCourse(value);
  };
  const changeGroup = (value) => {
    if (memberRadioChecked) {
      localstorage.groupId = value;
    }
    setGroup(value);
  };

  const MemberRadioPressed = () => {
    const currentIsChecked = !memberRadioChecked;

    if (currentIsChecked) {
      localstorage.isStored = true;
      localstorage.cafedraId = cafedra;
      localstorage.groupId = group;
      localstorage.courseId = course;
    } else {
      localstorage.isStored = false;
      localstorage.cafedraId = 0;
      localstorage.groupId = 0;
      localstorage.courseId = 0;
    }

    setMemberRadioChecked(currentIsChecked);
  };

  return (
    <SafeAreaView style={styles.container}>
      <PageHeader />
      <View style={styles.content_container}>
        <RNPickerSelect
          placeholder={{ label: "Выберите кафедру", value: null }}
          useNativeAndroidPickerStyle={true}
          style={{
            inputAndroid: {
              color: "black",
              margin: 5,
              borderWidth: 2,
              borderRadius: 2,
              borderColor: "lightgray",
              padding: 5,
            },
          }}
          value={cafedra}
          items={cafedraValues}
          onValueChange={(item) => changeCafedra(item)}
        ></RNPickerSelect>
        <View style={styles.pickers_contaner}>
          <View style={{ flex: 1 }}>
            <RNPickerSelect
              placeholder={{ label: "Выберите курс", value: null }}
              useNativeAndroidPickerStyle={true}
              style={{
                inputAndroid: {
                  color: "black",
                  margin: 5,
                  borderWidth: 2,
                  borderRadius: 2,
                  borderColor: "lightgray",
                  padding: 5,
                },
              }}
              value={course}
              items={courseValues}
              onValueChange={(item) => changeCourse(item)}
            ></RNPickerSelect>
          </View>
          <View style={{ flex: 1 }}>
            <RNPickerSelect
              placeholder={{ label: "Выберите группу", value: null }}
              useNativeAndroidPickerStyle={true}
              style={{
                inputAndroid: {
                  color: "black",
                  margin: 5,
                  borderWidth: 2,
                  borderRadius: 2,
                  borderColor: "lightgray",
                  padding: 5,
                },
              }}
              value={group}
              items={groupValues}
              onValueChange={(item) => changeGroup(item)}
            ></RNPickerSelect>
          </View>
        </View>
        <View>
          <RadioButton
            value="Запомнить"
            status={memberRadioChecked ? "checked" : "unchecked"}
            onPress={() => MemberRadioPressed()}
          />
        </View>
        <View style={styles.btn_contaner}>
          <View style={styles.btn_contaner_inner}>
            <Button
              style={styles.btn_load}
              title={"Загрузить"}
              onPress={onLoadPress}
            ></Button>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
    margin: 5,
    marginTop: 0,
  },
  content_container: {
    marginTop: 50,
    flex: 1,
    margin: 10,
  },
  pickers_contaner: {
    flexDirection: "row",
    width: "100%",
  },
  btn_contaner: {
    width: "100%",
    alignItems: "center",
  },
  btn_contaner_inner: {
    width: "100%",
  },

  picture: {
    width: 100,
    height: 100,
  },
  picker_cafedra: {
    height: 50,
    margin: 5,
    borderWidth: 2,
    borderRadius: 2,
    borderColor: "lightgray",
    padding: 5,
  },
  picker_item: {
    flex: 1,
    margin: 5,
    height: 50,
    borderWidth: 2,
    borderRadius: 2,
    borderColor: "lightgray",
    padding: 5,
  },
  btn_load: {
    opacity: 1,
    margin: 25,
    backgroundColor: "blue",
    height: 50,
  },
  btn_load_text: {
    fontSize: 15,
    backgroundColor: "white",
  },
});
