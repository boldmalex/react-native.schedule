import React from "react";
import { StyleSheet, SafeAreaView, Button, Text, Image } from "react-native";

export default function PageHeader() {
  return (
    <SafeAreaView style={styles.header_container}>
      <Image
        style={styles.header_picture}
        source={require("../assets/logo3.png")}
      />
      <Text style={styles.header_text}>Расписание</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header_container: {
    backgroundColor: "#33EF71",
    alignItems: "center",
    justifyContent: "center",
    height: 90,
    shadowColor: "#33EF71",
    shadowOpacity: 0.61,
    shadowOffset: { width: 0, height: 20 },
    shadowRadius: 2,
  },
  header_picture: {
    width: 35,
    height: 35,
    marginTop: 20,
  },
  header_text: {
    fontSize: 18,
    fontFamily: "c_gothic",
  },
});
