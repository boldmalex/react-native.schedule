export const timeData = [
  { id: 0, time: "8:30-9:55" },
  { id: 1, time: "10:05-11:30" },
  { id: 2, time: "11:40-13:05" },
  { id: 3, time: "13:45-15:10" },
  { id: 4, time: "15:20-16:45" },
  { id: 5, time: "16:55-18:20" },
];

export const dayValues = [
  { day: 1, value: "Понедельник" },
  { day: 2, value: "Вторник" },
  { day: 3, value: "Среда" },
  { day: 4, value: "Четверг" },
  { day: 5, value: "Пятница" },
  { day: 6, value: "Суббота" },
  { day: 7, value: "Воскресенье" },
];

export default timeData;
