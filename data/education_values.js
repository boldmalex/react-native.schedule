export const courseValues = [
  { label: "1 курс", value: "1" },
  { label: "2 курс", value: "2" },
  { label: "3 курс", value: "3" },
];

export const groupValues = [
  { label: "2-ТИД-2", value: "1" },
  { label: "2-ТИД-3", value: "2" },
];

export const cafedraValues = [
  { label: "ИПТиО", value: "1" },
  { label: "Другое", value: "2" },
];

export default groupValues;
