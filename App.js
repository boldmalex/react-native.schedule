import { StyleSheet, SafeAreaView } from "react-native";
import * as Font from "expo-font";
import { useState } from "react";
import AppLoading from "expo-app-loading";
import ScheduleChoise from "./components/schedule-choise";
import ScheduleShow from "./components/schedule-show";
import { NativeRouter, Route, Routes } from "react-router-native";

const loadFonts = () =>
  Font.loadAsync({
    rostov: require("./assets/fonts/rostov.ttf"),
    c_gothic: require("./assets/fonts/centurygothic.ttf"),
  });

export default function App() {
  const [isFontLoaded, setFontLoaded] = useState(false);

  if (isFontLoaded) {
    return (
      <NativeRouter>
        <SafeAreaView style={styles.container}>
          <Routes>
            <Route exact path="/" element={<ScheduleChoise />} />
            <Route path="/schedule/:idGroup" element={<ScheduleShow />} />
          </Routes>
        </SafeAreaView>
      </NativeRouter>
    );
  } else {
    return (
      <>
        <AppLoading
          startAsync={loadFonts}
          onFinish={() => setFontLoaded(true)}
          onError={() => console.log("error")}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  picture: {
    width: 100,
    height: 100,
  },
  text: {
    fontFamily: "rostov",
    fontSize: 18,
  },
});
